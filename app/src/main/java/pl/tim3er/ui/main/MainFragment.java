package pl.tim3er.ui.main;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import org.w3c.dom.Text;

import pl.tim3er.R;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    TextView textView;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        textView = getView().findViewById(R.id.message);
        super.onActivityCreated(savedInstanceState);
        try {
            mViewModel.getUsers().observe(this, users -> {
                textView.setText(users.toString() + " " + System.currentTimeMillis());
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // TODO: Use the ViewModel
    }

}
