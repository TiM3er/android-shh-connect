package pl.tim3er.ui.main;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.common.io.CharStreams;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class MainViewModel extends ViewModel {

    // TODO: Implement the ViewModel

    private MutableLiveData<List<String>> cpus;
    String data = "";

    public LiveData<List<String>> getUsers() throws InterruptedException {
        if (cpus == null)
            cpus = new MutableLiveData<List<String>>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                SendfeedbackJob sendfeedbackJob = new SendfeedbackJob();
                sendfeedbackJob.execute(new String[]{});
            }
        });
        thread.run();
        return cpus;
    }

    private
    class SendfeedbackJob extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            String CommandOutput = null;

            try {
                JSch jsch = new JSch();
                Session session = jsch.getSession("your login", "your host ", 22);

                session.setPassword("your password");

                // Avoid asking for key confirmation
                Properties prop = new Properties();
                prop.put("StrictHostKeyChecking", "no");
                session.setConfig(prop);

                session.connect();

                // SSH Channel

                Channel channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand("ls");
                channel.setInputStream(null);
                ((ChannelExec) channel).setErrStream(System.err);

                InputStream in = channel.getInputStream();

                channel.connect();
                byte[] tmp = new byte[1024];
                while (true) {
                    while (in.available() > 0) {
                        int i = in.read(tmp, 0, 1024);

                        if (i < 0)
                            break;
                        // System.out.print(new String(tmp, 0, i));
                        CommandOutput = new String(tmp, 0, i);
                    }

                    if (channel.isClosed()) {
                        // System.out.println("exit-status: " +
                        // channel.getExitStatus());
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ee) {
                    }
                }
                channel.disconnect();
                session.disconnect();
                // System.out.println("DONE");
                channel.disconnect();
                Log.e("test", CommandOutput + " " );
                List<String> strings = new LinkedList<>();
                strings.add(CommandOutput);
                cpus.postValue(strings);


            } catch (JSchException | IOException e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String message) {
            //process message
        }
    }
}
